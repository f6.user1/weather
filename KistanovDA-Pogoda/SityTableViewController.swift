//
//  SityTableViewController.swift
//  KistanovDA-Pogoda
//
//  Created by WSR on 6/20/19.
//  Copyright © 2019 WSR. All rights reserved.
//

import UIKit

class SityTableViewController: UITableViewController {
    var sityArray = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        sityArray.append("Москва")
        sityArray.append("Санкт-Петербург")
        sityArray.append("Оренбург")
        sityArray.append("Екатеринбург")
        sityArray.append("Samara")
        sityArray.append("Владивосток")
        tableView.backgroundView = UIImageView.init(image: UIImage(named: "backgrd"))
       
        tableView.reloadData()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }


    // MARK: - Table view data source

    @IBAction func AddSity(_ sender: Any) {
    let alert = UIAlertController(title: "Добавление города", message: "Добавьте, пожалуйста, город", preferredStyle: .alert)
        alert.addTextField{(textField) in textField.placeholder = "Добавьте город"}
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(alertAction) in
        let textField = alert.textFields![0] as UITextField
        self.sityArray.append(textField.text!)
        self.tableView.reloadData()
        }))
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: {(alertAction) in}))
        self.present(alert,animated: true, completion: nil)
    }
    



    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return sityArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SityCell", for: indexPath) as! SityTableViewCell
        cell.citycell.text = sityArray[indexPath.row]
        cell.backgroundColor = UIColor.clear

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */


    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            sityArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }


    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toMain" {
            let vc = segue.destination as! ViewController
            let index = self.tableView.indexPathForSelectedRow
            vc.city = sityArray[(index?.row)!]
        }
        
    }

}
