//
//  ViewController.swift
//  KistanovDA-Pogoda
//
//  Created by WSR on 6/21/19.
//  Copyright © 2019 WSR. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {
    @IBOutlet weak var citylabel: UILabel!
    @IBOutlet weak var citytemp: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var icoWeather: UIImageView!

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textFIeld: UITextField!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    var city = "Moscow"
    var temp = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFIeld.delegate = self
        searchImage(text: "russia")
        textFIeld.becomeFirstResponder()
        imageView.layer.masksToBounds = true
        
        downloadData(city: city)
        citylabel.text = city
        citytemp.text = temp
        
    }
    
    func convert(farm: Int, server: String, id:String, secret: String) -> URL? {
        return URL(string:"https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)_c.jpg")
    }
    
    func showError(text:String) {
        let alert = UIAlertController(title: "Упс...", message: text, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default)
        
        alert.addAction(ok)
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    func showLoader(show:Bool){
        DispatchQueue.main.async {
            if(show){
                self.imageView.image = nil
                self.loader.startAnimating()
            }
            else {
                self.loader.stopAnimating()
            }
        }
    }
    

        func searchImage(text: String){
            showLoader(show: true)
            let base = "https://api.flickr.com/services/rest/?method=flickr.photos.search"
            let key = "&api_key=154ca59f46d012b1959cf186fc724d2f"
            let format = "&format=json&nojsoncallback=1"
            let farmattedText = text.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? ""
            let textToSearch = "&text=\(farmattedText)"
            let sort = "&sort=relevance"
            let searchUrl = base + key + textToSearch + sort + format;
            let url = URL(string: searchUrl)!
            URLSession.shared.dataTask(with: url) { (data, _, _) in
                guard let jsonData = data else{
                    self.showLoader(show: false)
                    self.showError(text:"Error, json is empty")
                    return
                }
                guard let jsonAny = try? JSONSerialization.jsonObject(with: jsonData, options: []) else{
                    self.showLoader(show: false)
                    return
                }
                guard let json = jsonAny as? [String: Any] else {
                    self.showLoader(show: false)
                    return
                }
                
                guard let photos = json["photos"] as? [String: Any] else {
                    self.showLoader(show: false)
                    return
                }
                
                guard let photosArray = photos["photo"] as? [Any] else {
                    self.showLoader(show: false)
                    return
                }
                
                guard photosArray.count > 0 else {
                    self.showLoader(show: false)
                    self.showError(text:"Photos is empty")
                    return
                }
                guard let firstPhoto = photosArray[0] as? [String: Any] else {
                    self.showLoader(show: false)
                    return
                }
                let farm = firstPhoto["farm"] as! Int
                let id = firstPhoto["id"] as! String;
                let secret = firstPhoto["secret"] as! String;
                let server = firstPhoto["server"] as! String;
                
                let pictureUrl = self.convert(farm: farm, server: server, id: id, secret: secret)
                
                URLSession.shared.dataTask(with: pictureUrl!, completionHandler: { (data, _, _) in
                    DispatchQueue.main.async {
                        self.imageView.image = UIImage(data:data!)
                    }
                    self.showLoader(show: false)
                }).resume()
                }.resume()
        }


    
    func downloadData(city: String){
        var prtemp = 0.0
 
        let token = "d827fff2c3d14e37ebf57321e477e96f"
        let urlStr = "https://api.openweathermap.org/data/2.5/weather?q=\(city)&units=metric&appid=\(token)"
        let url = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let numberFormatter: NumberFormatter = {
            let nf = NumberFormatter()
            nf.numberStyle = .decimal
            nf.minimumFractionDigits = 0
            nf.maximumFractionDigits = 1
            return nf
        }()
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        Alamofire.request(url!, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                //если запрос выполнен успешно, то разбираем ответ и вытаскиваем нужные данные
                let json = JSON(value)
                prtemp = json["main"]["temp"].doubleValue
                numberFormatter.string(from: NSNumber(value: prtemp))
                self.citytemp.text = String(prtemp)
                self.time.text = String(hour)+":"+String(minute)
                let icoName = json["weather"][0]["icon"].stringValue
                
                if let urlStr2 = URL(string: "https://openweathermap.org/img/w/\(icoName).png") {
                if let data = try? Data(contentsOf: urlStr2){
                    self.icoWeather.image = UIImage(data: data)
                    }
                }
                
            case .failure(let error):
                print(error)
            }


        }
    }
    
}
    extension ViewController: UITextFieldDelegate {
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            searchImage(text: textField.text!)
            return true
        }
    }

